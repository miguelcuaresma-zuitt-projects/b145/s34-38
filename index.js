//npm init -y
//npm i nodemon cors mongoose express -y

//dependencies

	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	const userRouter = require("./routes/userRoutes");
	const courseRoutes = require("./routes/courseRoutes")
	const dotenv = require("dotenv");

//environment variables setup
	//configure the application in order for it to recognize and identify the necessary components needed to build the app successfully

	dotenv.config();
	//extract the variables from the .env file
	const secret = process.env.CONNECTION_STRING

//server setup
	const app = express();
	const port = process.env.PORT || 4000;

//database connect	
	mongoose.connect(secret, {
		useNewUrlParser: true,
		useUnifiedTopology :true
	});

//middlewares

	app.use(express.json());
	app.use(express.urlencoded({extended: true}));
	app.use(cors());

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Connection error"));
	db.once("open", () => console.log("Successfully connected to MongoDB"));

//Server routes
	app.use("/users", userRouter);
	app.use("/courses", courseRoutes);

//Server response
	app.listen(port, () => {
		console.log(`Server is running at port ${port}`);
	});
