const Course = require("../models/Course");
const auth = require("../auth.js")
const courseData = require("../routes/courseRoutes.js");

// add a course

module.exports.addCourse = (reqBody, courseData) => {

	if (courseData.isAdmin) {
		let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newCourse.save().then((user, err) => {
		if (err) {
			return false;
		} else {
			return true;
		};
	});
	
	} else {
		return Promise.resolve("Admin verification required.")
	}
};
//retrieving all courses


module.exports.getAllCourses = async (user) => {

	if(user.isAdmin === true){

		return Course.find({}).then(result => {

			return result
		})
		
	} else {

		return `${user.email} is not authorized`
	}
}


// retrieval of active courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// retrieval of a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// updating a course
//find - authenticate - save
module.exports.updateCourse = (data) => {

	return Course.findById(data.courseId).then((result,err) => {
		console.log(result)
		if(data.payload.isAdmin === true){
		
				result.name = data.updatedCourse.name;
				result.description = data.updatedCourse.description;
				result.price = data.updatedCourse.price;
	
			return result.save().then((updatedCourse, err) => {
				if(err){
					return false;
				} else {
					return updatedCourse;
				}
			})
		} else {
			return false;
		}
	})
}


module.exports.archiveCourse = async (data) => {
	return Course.findById(data.courseId).then((result, err) => {
		if(data.payload.isAdmin) {
			result.isActive = false;
			return result.save().then((archivedCourse, err) =>{
				if (err) {
					return false;
				} else {
					return archivedCourse;
				}
			})
		} else {
			return `Administrative authentication required.`;
		};
	});
};