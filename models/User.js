const mongoose = require("mongoose");

const User = new mongoose.Schema({
	firstName : {
		type : String,
		required: [true, "First name is required"]
	},
	lastName : {
		type : String,
		required: [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "E-mail is required"]
	},
	password : {
		type : String,
		required: [true, "Please enter a valid password"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required: [true, "Please enter a valid mobile number"]
	},
	enrollments : [
		{
			courseId : 
				{
					type : String
				}
		},
		{
			enrolledOn : {
				type : Date,
				default : new Date()
			}
		},
		{
			status : {
				type : String,
				default : "Enrolled"
			}
		}
	]
});

module.exports = mongoose.model("User", User);