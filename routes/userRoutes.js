//modules
	const express = require("express");
	const router = express.Router();
	const userController = require("../controllers/userController")
	const auth = require("../auth");
	const Course = require("../models/Course")

// Checking email
	router.post("/checkEmail", (req, res) => {
		userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
	});
// registration
	router.post("/register", (req, res) => {
		userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
	});

// Login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

//ACTIVITY

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

/*
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};
*/

//get all users

	router.get('/userdb', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	userController.getAllUsers(data).then(resultFromController => res.send(resultFromController))
});


//enroll route
	router.post("/enroll", auth.verify, (req, res) => {

		let data = {
			payload : auth.decode(req.headers.authorization),
			userId : req.body.userId,
			courseId : req.body.courseId
		};

		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	});



module.exports = router;